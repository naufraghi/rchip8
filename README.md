# CHIP-8 emulator in Rust (WIP)

CHIP-8 is an interpreted virtual machine (in the sense no hardware ever existed), but the
architecture is similar enough to the real ones you can find around in the 70s to be and
educative exercise.

## References
- [Wikipedia: CHIP-8](http://en.wikipedia.org/wiki/CHIP-8), the reference
- [How to write an emulator (CHIP-8 interpreter)](http://www.multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/), a simple architecture
- [Building a Chip-8 Interpreter in C#](https://blog.dantup.com/2016/06/building-a-chip-8-interpreter-in-csharp/), some more structured architechture
- [Writing a CHIP-8 emulator with Rust and WebAssembly](https://blog.scottlogic.com/2017/12/13/chip8-emulator-webassembly-rust.html), some
  focus on Rust pattern matching and WASM integration
- [Cowgod's Chip-8 Technical Reference v1.0](http://devernay.free.fr/hacks/chip8/C8TECH10.HTM)
