//! # CHIP-8 emulator in Rust (WIP)
//!
//! CHIP-8 is an interpreted virtual machine (in the sense no hardware ever existed), but the
//! architecture is similar enough to the real ones you can find around in the 70s to be and
//! educative exercise.
//!
//! ## References
//! - [Wikipedia: CHIP-8](http://en.wikipedia.org/wiki/CHIP-8), the reference
//! - [How to write an emulator (CHIP-8 interpreter)](http://www.multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/), a simple architecture
//! - [Building a Chip-8 Interpreter in C#](https://blog.dantup.com/2016/06/building-a-chip-8-interpreter-in-csharp/), some more structured architechture
//! - [Writing a CHIP-8 emulator with Rust and WebAssembly](https://blog.scottlogic.com/2017/12/13/chip8-emulator-webassembly-rust.html), some
//!   focus on Rust pattern matching and WASM integration
//! - [Cowgod's Chip-8 Technical Reference v1.0](http://devernay.free.fr/hacks/chip8/C8TECH10.HTM)
//!
//! ## Decode the instruction
//!
//! The first part of this emulation project that triggers my curiosity is how can we make a good
//! decoding sequence that is both clear and fast, using some features Rust offers.
//!
//! We will likely have a memory buffer, that will be perhaps the common `memory = [u8; 4096]`
//! array that most implementation ar using.
//!
//! Each instruction is composed by two bytes, `0xAB | 0xCD`, we'd like to read as `0xABCD`, intead
//! of shifting bit ourself the 1.32 version of Rust has the utility method
//! `u16::from_be_bytes([u8; 2])` that does if for you, documenting you are in a Big Endian world.
//!
//! We may not need to have a single word, we may keep our two bytes as they are, but nevertheless,
//! we need to map to some operation that may have some values, I'll cut-&-paste the opcode
//! referece for easier access.
//!
//! ```hex
//! 00E0 - CLS
//! 00EE - RET
//! 0nnn - SYS addr
//! 1nnn - JP addr
//! 2nnn - CALL addr
//! 3xkk - SE Vx, byte
//! 4xkk - SNE Vx, byte
//! 5xy0 - SE Vx, Vy
//! 6xkk - LD Vx, byte
//! 7xkk - ADD Vx, byte
//! 8xy0 - LD Vx, Vy
//! 8xy1 - OR Vx, Vy
//! 8xy2 - AND Vx, Vy
//! 8xy3 - XOR Vx, Vy
//! 8xy4 - ADD Vx, Vy
//! 8xy5 - SUB Vx, Vy
//! 8xy6 - SHR Vx {, Vy}
//! 8xy7 - SUBN Vx, Vy
//! 8xyE - SHL Vx {, Vy}
//! 9xy0 - SNE Vx, Vy
//! Annn - LD I, addr
//! Bnnn - JP V0, addr
//! Cxkk - RND Vx, byte
//! Dxyn - DRW Vx, Vy, nibble
//! Ex9E - SKP Vx
//! ExA1 - SKNP Vx
//! Fx07 - LD Vx, DT
//! Fx0A - LD Vx, K
//! Fx15 - LD DT, Vx
//! Fx18 - LD ST, Vx
//! Fx1E - ADD I, Vx
//! Fx29 - LD F, Vx
//! Fx33 - LD B, Vx
//! Fx55 - LD [I], Vx
//! Fx65 - LD Vx, [I]
//! ```
//!
//! And this seems a perfect fit for an `enum`, and we may start writing some warmup test, to find
//! a good decoding structure.
//!

use std::fmt;

#[derive(Debug, PartialEq, Eq)]
pub enum Op {
    /// 00E0 - CLS
    Cls,
    /// 00EE - RET
    Ret,
    /// 0nnn - SYS addr
    Sys(Addr),
    /// 1nnn - JP addr
    Jp(Addr),
    /// 2nnn - CALL addr
    Call(Addr),
    /// 3xkk - SE Vx, byte
    Se(Vx, Byte),
    /// 4xkk - SNE Vx, byte
    Sne(Vx, Byte),
    /// 5xy0 - SE Vx, Vy
    Sev(Vx, Vx),
    /// 6xkk - LD Vx, byte
    Ld(Vx, Byte),
    /// 7xkk - ADD Vx, byte
    Add(Vx, Byte),
    /// 8xy0 - LD Vx, Vy
    Ldv(Vx, Vx),
    /// 8xy1 - OR Vx, Vy
    Or(Vx, Vx),
    /// 8xy2 - AND Vx, Vy
    And(Vx, Vx),
    /// 8xy3 - XOR Vx, Vy
    Xor(Vx, Vx),
    /// 8xy4 - ADD Vx, Vy
    Addv(Vx, Vx),
    /// 8xy5 - SUB Vx, Vy
    Sub(Vx, Vx),
    /// 8xy6 - SHR Vx {, Vy}
    Shr(Vx),
    /// 8xy7 - SUBN Vx, Vy
    Subn(Vx, Vx),
    /// 8xyE - SHL Vx {, Vy}
    Shl(Vx),
    /// 9xy0 - SNE Vx, Vy
    Snev(Vx, Vx),
    /// Annn - LD I, addr
    Ldi(Addr),
    /// Bnnn - JP V0, addr
    Jpv0(Addr),
    /// Cxkk - RND Vx, byte
    Rnd(Vx, Byte),
    /// Dxyn - DRW Vx, Vy, nibble
    Drw(Vx, Vx, Nibble),
    /// Ex9E - SKP Vx
    Skp(Vx),
    /// ExA1 - SKNP Vx
    Skpn(Vx),
    /// Fx07 - LD Vx, DT
    Ldt(Vx),
    /// Fx0A - LD Vx, K
    Ldk(Vx),
    /// Fx15 - LD DT, Vx
    Sdt(Vx),
    /// Fx18 - LD ST, Vx
    Sst(Vx),
    /// Fx1E - ADD I, Vx
    Addi(Vx),
    /// Fx29 - LD F, Vx
    Ldf(Vx),
    /// Fx33 - LD B, Vx
    Ldb(Vx),
    /// Fx55 - LD [I], Vx
    Ldiv(Vx),
    /// Fx65 - LD Vx, [I]
    Ldvi(Vx),
}

impl Op {
    pub fn decode(n1: u8, n2: u8) -> Op {
        //! We can than start decoding the bytes, given there are a few fixed nibbles, the first and sometime
        //! the last one or two, it's easier to match on the two separated bytes that on a single 16 bit
        //! value.
        match (n1, n2) {
            /* 00E0 - CLS                */ (0x00, 0xE0) => Op::Cls,
            /* 00EE - RET                */ (0x00, 0xEE) => Op::Ret,
            /* 0nnn - SYS addr           */
            (0x00...0x0F, _) => Op::Sys(Addr(u16::from_be_bytes([n1 & 0x0F, n2]))),
            /* 1nnn - JP addr            */
            (0x10...0x1F, _) => Op::Jp(Addr(u16::from_be_bytes([n1 & 0x0F, n2]))),
            /* 2nnn - CALL addr          */
            (0x20...0x2F, _) => Op::Call(Addr(u16::from_be_bytes([n1 & 0x0F, n2]))),
            /* 3xkk - SE Vx, byte        */
            (0x30...0x3F, _) => Op::Se(Vx(n1 & 0x0F), Byte(n2)),
            /* 4xkk - SNE Vx, byte       */
            (0x40...0x4F, _) => Op::Sne(Vx(n1 & 0x0F), Byte(n2)),
            /* 5xy0 - SE Vx, Vy          */
            (0x50...0x5F, 0x00...0xF0) => Op::Sev(Vx(n1 & 0x0F), Vx(n2 >> 4)),
            /* 6xkk - LD Vx, byte        */
            (0x60...0x6F, _) => Op::Ld(Vx(n1 & 0x0F), Byte(n2)),
            /* 7xkk - ADD Vx, byte       */
            (0x70...0x7F, _) => Op::Add(Vx(n1 & 0x0F), Byte(n2)),
            /* 8xy0 - LD Vx, Vy          */
            (0x80...0x8F, 0x00...0xF0) => Op::Ldv(Vx(n1 & 0x0F), Vx(n2 >> 4)),
            /* 8xy1 - OR Vx, Vy          */
            (0x80...0x8F, 0x01...0xF1) => Op::Or(Vx(n1 & 0x0F), Vx(n2 >> 4)),
            /* 8xy2 - AND Vx, Vy         */
            (0x80...0x8F, 0x02...0xF2) => Op::And(Vx(n1 & 0x0F), Vx(n2 >> 4)),
            /* 8xy3 - XOR Vx, Vy         */
            (0x80...0x8F, 0x03...0xF3) => Op::Xor(Vx(n1 & 0x0F), Vx(n2 >> 4)),
            /* 8xy4 - ADD Vx, Vy         */
            (0x80...0x8F, 0x04...0xF4) => Op::Addv(Vx(n1 & 0x0F), Vx(n2 >> 4)),
            /* 8xy5 - SUB Vx, Vy         */
            (0x80...0x8F, 0x05...0xF5) => Op::Sub(Vx(n1 & 0x0F), Vx(n2 >> 4)),
            /* 8xy6 - SHR Vx {, Vy}      */
            (0x80...0x8F, 0x06...0xF6) => Op::Shr(Vx(n1 & 0x0F)),
            /* 8xy7 - SUBN Vx, Vy        */
            (0x80...0x8F, 0x07...0xF7) => Op::Subn(Vx(n1 & 0x0F), Vx(n2 >> 4)),
            /* 8xyE - SHL Vx {, Vy}      */
            (0x80...0x8F, 0x0E...0xFE) => Op::Shl(Vx(n1 & 0x0F)),
            /* 9xy0 - SNE Vx, Vy         */
            (0x90...0x9F, 0x00...0xF0) => Op::Snev(Vx(n1 & 0x0F), Vx(n2 >> 4)),
            /* Annn - LD I, addr         */
            (0xA0...0xAF, _) => Op::Ldi(Addr(u16::from_be_bytes([n1 & 0x0F, n2]))),
            /* Bnnn - JP V0, addr        */
            (0xB0...0xBF, _) => Op::Jpv0(Addr(u16::from_be_bytes([n1 & 0x0F, n2]))),
            /* Cxkk - RND Vx, byte       */
            (0xC0...0xCF, _) => Op::Rnd(Vx(n1 & 0x0F), Byte(n2)),
            /* Dxyn - DRW Vx, Vy, nibble */
            (0xD0...0xDF, _) => Op::Drw(Vx(n1 & 0x0F), Vx(n2 >> 4), Nibble(n2 & 0x0F)),
            /* Ex9E - SKP Vx             */
            (0xE0...0xEF, 0x9E...0x9E) => Op::Skp(Vx(n1 & 0x0F)),
            /* ExA1 - SKNP Vx            */
            (0xE0...0xEF, 0xA1...0xA1) => Op::Skpn(Vx(n1 & 0x0F)),
            /* Fx07 - LD Vx, DT          */
            (0xF0...0xFF, 0x07...0x07) => Op::Ldt(Vx(n1 & 0x0F)),
            /* Fx0A - LD Vx, K           */
            (0xF0...0xFF, 0x0A...0x0A) => Op::Ldk(Vx(n1 & 0x0F)),
            /* Fx15 - LD DT, Vx          */
            (0xF0...0xFF, 0x15...0x15) => Op::Sdt(Vx(n1 & 0x0F)),
            /* Fx18 - LD ST, Vx          */
            (0xF0...0xFF, 0x18...0x18) => Op::Sst(Vx(n1 & 0x0F)),
            /* Fx1E - ADD I, Vx          */
            (0xF0...0xFF, 0x1E...0x1E) => Op::Addi(Vx(n1 & 0x0F)),
            /* Fx29 - LD F, Vx           */
            (0xF0...0xFF, 0x29...0x29) => Op::Ldf(Vx(n1 & 0x0F)),
            /* Fx33 - LD B, Vx           */
            (0xF0...0xFF, 0x33...0x33) => Op::Ldb(Vx(n1 & 0x0F)),
            /* Fx55 - LD [I], Vx         */
            (0xF0...0xFF, 0x55...0x55) => Op::Ldiv(Vx(n1 & 0x0F)),
            /* Fx65 - LD Vx, [I]         */
            (0xF0...0xFF, 0x65...0x65) => Op::Ldvi(Vx(n1 & 0x0F)),
            _ => unreachable!(format!("Unhandled opcode 0x{:X}{:X}", n1, n2)),
        }
    }
}

#[derive(PartialEq, Eq)]
pub struct Addr(pub u16);

impl fmt::Debug for Addr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Addr(0x{:X})", self.0)
    }
}

#[derive(PartialEq, Eq)]
pub struct Byte(pub u8);

impl fmt::Debug for Byte {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Byte(0x{:X})", self.0)
    }
}

#[derive(PartialEq, Eq)]
pub struct Nibble(pub u8);

impl fmt::Debug for Nibble {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Nibble(0x{:X})", self.0)
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct Vx(pub u8); // For Vx decimal values are more intuitive

#[test]
fn test_parse_opcode_jp() {
    // 1nnn - JP addr
    let (n1, n2) = (0x11, 0x23);
    // we need to put a mask on the first nibble, to remove the opcode fixed part
    let addr = Addr(u16::from_be_bytes([n1 & 0x0F, n2]));
    // This may in fact cost more than masking an u16, need to chech generated asm
    let op = Op::Jp(addr);
    println!("Decoding 0x{:X}{:X} -> {:?}", n1, n2, op);
    assert_eq!(format!("{:?}", op), "Jp(Addr(0x123))");
    assert_eq!(op, Op::decode(n1, n2));
}

#[test]
fn test_parse_opcode_se() {
    // 3xkk - SE Vx, byte
    let (n1, n2) = (0x34, 0x56);
    let x = n1 & 0x0F;
    let op = Op::Se(Vx(x), Byte(n2));
    println!("Decoding 0x{:X}{:X} -> {:?}", n1, n2, op);
    assert_eq!(format!("{:?}", op), "Se(Vx(4), Byte(0x56))");
    assert_eq!(op, Op::decode(n1, n2));
}

#[test]
fn test_parse_opcode_sev() {
    // 5xy0 - SE Vx, Vy
    let (n1, n2) = (0x54, 0x30);
    let op = Op::Sev(Vx(n1 & 0x0F), Vx(n2 >> 4));
    println!("Decoding 0x{:X}{:X} -> {:?}", n1, n2, op);
    assert_eq!(format!("{:?}", op), "Sev(Vx(4), Vx(3))");
    assert_eq!(op, Op::decode(n1, n2));
}

#[test]
fn test_parse_opcode_drw() {
    // Dxyn - DRW Vx, Vy, nibble
    let (n1, n2) = (0xD1, 0x23);
    let op = Op::Drw(Vx(n1 & 0x0F), Vx(n2 >> 4), Nibble(n2 & 0x0F));
    println!("Decoding 0x{:X}{:X} -> {:?}", n1, n2, op);
    assert_eq!(format!("{:?}", op), "Drw(Vx(1), Vx(2), Nibble(0x3))");
    assert_eq!(op, Op::decode(n1, n2));
}
