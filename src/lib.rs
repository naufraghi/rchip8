//! # CHIP-8 emulator in Rust (WIP)
//!
//! CHIP-8 is an interpreted virtual machine (in the sense no hardware ever existed), but the
//! architecture is similar enough to the real ones you can find around in the 70s to be and
//! educative exercise.
//!
//! ## References
//! - [Wikipedia: CHIP-8](http://en.wikipedia.org/wiki/CHIP-8), the reference
//! - [How to write an emulator (CHIP-8 interpreter)](http://www.multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/), a simple architecture
//! - [Building a Chip-8 Interpreter in C#](https://blog.dantup.com/2016/06/building-a-chip-8-interpreter-in-csharp/), some more structured architechture
//! - [Writing a CHIP-8 emulator with Rust and WebAssembly](https://blog.scottlogic.com/2017/12/13/chip8-emulator-webassembly-rust.html), some
//!   focus on Rust pattern matching and WASM integration
//! - [Cowgod's Chip-8 Technical Reference v1.0](http://devernay.free.fr/hacks/chip8/C8TECH10.HTM)
//!
mod decode;

use decode::{Addr, Byte, Nibble, Op, Vx};
use rand::{RngCore, SeedableRng};
use rand_xorshift::XorShiftRng;
use std::fmt;

/// The keypad has 16 possible states, 0x0 ~ 0xF
struct Keypad(u8);

/// CHIP-8 Fonteset
static CHIP8_FONTSET: [u8; 80] = [
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80, // F
];

/// The display is a 64x32 monochrome canvas
struct Screen {
    pixels: [u8; 8 * 32],
}

impl Screen {
    fn new() -> Screen {
        const pixels: usize = Screen::width() * Screen::height() / 8;
        Screen {
            pixels: [0; pixels],
        }
    }
    const fn width() -> usize {
        64
    }
    const fn height() -> usize {
        32
    }
    fn clear(&mut self) {
        self.pixels = Screen::new().pixels;
    }
    fn draw(&mut self, cpu: &Cpu, (x, y): (u8, u8), n: u8) -> bool {
        //! Draw `n` lines of the sprite pointed by `cpu.i`, at position (x, y)
        //!
        //!    01235678      xy=(0,0), n=1
        //! 0 |.OO.....|    |00..|    |0.0.|  <-- 1 xorred bit
        //! 1 |O..O....|    |....|    |....|
        //! 2 |O..O....| -> |....| -> |....|
        //! 3 |O..O....|    |....|    |....|
        //! 4 |.OO.....|    |....|    |....|
        //!
        let mut collision = false;
        for yline in 0..n as u16 {
            let memory_pos = (cpu.i + yline) as usize;
            let sprite_line = cpu.memory[memory_pos];
            let screen_pos = ((x as u16) + (y as u16 + yline) * 8) as usize;
            let screen_line = self.pixels[screen_pos];
            collision |=
                (screen_line | sprite_line).count_ones() > (screen_line ^ sprite_line).count_ones();
            self.pixels[screen_pos] = screen_line ^ sprite_line;
        }
        collision
    }
}

impl fmt::Display for Screen {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.pixels[..]
            .chunks(8)
            .enumerate()
            .map(|(c, line)| {
                let pixel_line = line
                    .iter()
                    .map(|i| format!("{:08b}", i))
                    .collect::<String>()
                    .replace('1', "X")
                    .replace('0', ".");
                if c + 1 < Screen::height() {
                    writeln!(f, "{}", pixel_line)
                } else {
                    write!(f, "{}", pixel_line)
                }
            })
            .collect()
    }
}

#[test]
fn test_screen_display() {
    let screen = Screen::new();
    println!("{}", screen);
}

struct Stack {
    pointers: Vec<u16>,
}

impl Stack {
    fn new() -> Stack {
        let pointers = Vec::with_capacity(16);
        Stack { pointers }
    }
    fn push(&mut self, p: u16) {
        self.pointers.push(p)
    }
    fn pop(&mut self) -> u16 {
        self.pointers.pop().expect("The stack is empty")
    }
}

struct Cpu {
    /// index register
    i: u16,
    /// program counter
    pc: u16,
    /// memory
    memory: [u8; 4096],
    /// registers
    v: [u8; 16],
    /// peripherals
    keypad: Keypad,
    display: Screen,
    /// stack
    stack: Stack,
    /// delay timer
    delay_timer: u8,
    /// sound timer
    sound_timer: u8,
    /// random
    random: XorShiftRng,
}

impl Cpu {
    fn new() -> Cpu {
        Cpu {
            i: 0,
            pc: 0x200,
            memory: {
                let mut memory = [0; 4096];
                memory[..80].clone_from_slice(&CHIP8_FONTSET);
                memory
            },
            v: [0; 16],
            keypad: Keypad(0),
            display: Screen::new(),
            stack: Stack::new(),
            delay_timer: 0,
            sound_timer: 0,
            random: XorShiftRng::seed_from_u64(42),
        }
    }
    fn fetch(&self) -> Op {
        let pc: usize = self.pc.into();
        Op::decode(self.memory[pc], self.memory[pc + 1])
    }
    fn increment(&mut self, count: u16) {
        self.pc += count * 2;
    }
    fn execute(mut self, op: Op) -> Self {
        use Op::*;
        match op {
            Cls => self.display.clear(),
            Ret => self.pc = self.stack.pop(),
            Jp(Addr(addr)) => self.pc = addr,
            Call(Addr(addr)) => {
                self.stack.push(self.pc);
                self.pc = addr
            }
            Se(Vx(vx), Byte(byte)) => {
                if self.v[vx as usize] == byte {
                    self.increment(2) // FIXME: how we manage single/no increments?
                }
            }
            Sne(Vx(vx), Byte(byte)) => {
                if self.v[vx as usize] != byte {
                    self.increment(2)
                }
            }
            Sev(Vx(x), Vx(y)) => {
                if self.v[x as usize] == self.v[y as usize] {
                    self.increment(2)
                }
            }
            Ld(Vx(x), Byte(byte)) => self.v[x as usize] = byte,
            Add(Vx(x), Byte(byte)) => self.v[x as usize] = self.v[x as usize].wrapping_add(byte),
            Ldv(Vx(x), Vx(y)) => self.v[x as usize] = self.v[y as usize],
            Or(Vx(x), Vx(y)) => self.v[x as usize] |= self.v[y as usize],
            And(Vx(x), Vx(y)) => self.v[x as usize] &= self.v[y as usize],
            Xor(Vx(x), Vx(y)) => self.v[x as usize] ^= self.v[y as usize],
            Sub(Vx(x), Vx(y)) => {
                let vx = self.v[x as usize];
                let vy = self.v[y as usize];
                self.v[0xF] = if vx > vy { 1 } else { 0 };
                self.v[x as usize] = vy.wrapping_sub(vx);
            }
            Shr(Vx(x)) => {
                self.v[0xF] = self.v[x as usize] & 0x0001;
                self.v[x as usize] = self.v[x as usize].wrapping_div(2);
            }
            Subn(Vx(x), Vx(y)) => {
                let vx = self.v[x as usize];
                let vy = self.v[y as usize];
                self.v[0xF] = if vy > vx { 1 } else { 0 };
                self.v[x as usize] = vx.wrapping_sub(vy);
            }
            Shl(Vx(x)) => {
                self.v[0xF] = self.v[x as usize] & 0x80; // 0b1000...
                self.v[x as usize] = self.v[x as usize].wrapping_mul(2);
            }
            Snev(Vx(x), Vx(y)) => {
                if self.v[x as usize] != self.v[y as usize] {
                    self.increment(2)
                }
            }
            Ldi(Addr(addr)) => self.i = addr,
            Jpv0(Addr(addr)) => self.pc = addr.wrapping_add(self.v[0].into()), // should panic on overflow?
            Rnd(Vx(x), Byte(byte)) => self.v[x as usize] = self.random.next_u32() as u8 & byte,
            _ => unimplemented!("Missing opcode"),
        };
        self
    }
    fn cycle(self) -> Self {
        let op = self.fetch();
        self
    }
}
